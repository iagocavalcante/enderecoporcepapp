<p align="center">
<a href="https://travis-ci.org/chrisbenseler/enderecoporcepapp">
<img src="https://api.travis-ci.org/chrisbenseler/enderecoporcepapp.svg?branch=master">
</a>
<a href="https://coveralls.io/github/chrisbenseler/enderecoporcepapp?branch=master">
<img src="https://coveralls.io/repos/github/chrisbenseler/enderecoporcepapp/badge.svg?branch=master">
</a>
</p>


# Endereço por CEP APP

Ionic 2 app that allows users to search addresses in Brazil through it's CEP (zipcode), via [CEP Promise](https://github.com/filipedeschamps/cep-promise) library
